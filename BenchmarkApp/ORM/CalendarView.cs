﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;

namespace BenchmarkApp.ORM
{
    public class CalendarView : DbTable
    {
        public static string TABLE_NAME = "events_on_date_cache";

        public static string CALENDAR_SQL = @"
with dts as (
    select :dt_from + generate_series as date
    from generate_series(0, :dt_to - :dt_from)
)
select distinct dts.date, ev.id as event_id
from dts, event as ev
         inner join event_meta as evm
                    on ev.id = evm.event_id
                        and evm.meta_key in ('repeat_start', 'repeat_once')
         left join event_meta as evre
                   on evre.meta_id = evm.id
                       and evre.meta_key = 'repeat_end'
         left join event_meta as evrwd
                   on evrwd.meta_id = evm.id
                       and evrwd.meta_key = 'repeat_weekday'
where (
        evm.meta_key = 'repeat_once'
        and evm.meta_value::date = dts.date
    )
   or (
        evm.meta_key = 'repeat_start'
        and evm.meta_value::date <= dts.date
        and (evre.meta_value is null or evre.meta_value::date >= dts.date)
        and (evrwd.meta_value is null or evrwd.meta_value::int = extract(isodow from dts.date))
    );
";
        public static string CALENDAR_CACHE_SQL = @"
select date, event_id
from events_on_date_cache
where date between :dt_from and :dt_to
";

        // Mechanism for cached interval determination undefined
        private static DateTime CACHE_BEGIN = new DateTime(2018, 10, 01);
        private static DateTime CACHE_END = new DateTime(2019, 03, 01);

        private static DateTime TEST_BEGIN = new DateTime(2018, 09, 01);
        private static DateTime TEST_END = new DateTime(2019, 04, 01);

        public CalendarView(Database database) : base(database, TABLE_NAME)
        {
        }

        /**
         * q18
         * Get Event IDs on specified date
         */
        public ICollection<int> Load(DateTime date, bool ignoreCache = false)
        {
            bool cacheHit = !ignoreCache && date >= CACHE_BEGIN && date <= CACHE_END;
            mDatabase.Connect();
            var command = mDatabase.CreateCommand(cacheHit ? CALENDAR_CACHE_SQL : CALENDAR_SQL);
            command.Parameters.AddWithValue(":dt_from", NpgsqlDbType.Date, date);
            command.Parameters.AddWithValue(":dt_to", NpgsqlDbType.Date, date);
            var reader = mDatabase.Select(command);
            var eventIds = Read(reader);
            reader.Close();
            mDatabase.Close();
            return eventIds;
        }

        public ICollection<int> TestLoadNoCache()
        {
            var random = new Random();
            var date = new DateTime(TEST_BEGIN.Ticks)
                .AddDays(random.Next(0, (int)(TEST_END - TEST_BEGIN).TotalDays));
            return Load(date, true);
        }

        public ICollection<int> TestLoadCache()
        {
            var random = new Random();
            var date = new DateTime(TEST_BEGIN.Ticks)
                .AddDays(random.Next(0, (int)(TEST_END - TEST_BEGIN).TotalDays));
            return Load(date);
        }

        private ICollection<int> Read(NpgsqlDataReader reader)
        {
            int eventIdCol = reader.GetOrdinal("event_id");
            var eventIds = new List<int>();
            while (reader.Read())
            {
                eventIds.Add(reader.GetInt32(eventIdCol));
            }
            return eventIds;
        }
    }
}
