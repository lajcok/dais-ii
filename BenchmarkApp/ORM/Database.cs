using System;
using System.Data;
using System.Web.Configuration;
using Npgsql;

namespace BenchmarkApp.ORM
{
    public class Database
    {
        // private static int TIMEOUT = 240;
        private NpgsqlConnection mConnection;
        uint mConnectNumber = 0;
        bool mTransactionFlag = false;
        NpgsqlTransaction mSqlTransaction;
        private String mLanguage = "en";

        public EventTable EventTable;
        public CalendarView CalendarView;

        public Database() 
        {
            mConnection = new NpgsqlConnection();
            EventTable = new EventTable(this);
            CalendarView = new CalendarView(this);
        }

        /**
         * Connect.
         **/
        public bool Connect(String conString)
        {
            if (!mTransactionFlag)
            {
                mConnectNumber++;
            }
            if (mConnection.State != System.Data.ConnectionState.Open)
            {
                mConnection.ConnectionString = conString;
                mConnection.Open();
            }
            return true;
        }

        /**
         * Connect.
         **/
        public bool Connect()
        {
            bool ret = true;

            if (mConnection.State != System.Data.ConnectionState.Open && WebConfigurationManager.ConnectionStrings["Default"] != null)
            {
                ret = Connect(WebConfigurationManager.ConnectionStrings["Default"].ConnectionString);
            }
            else if (!mTransactionFlag)
            {
                mConnectNumber++;
            }

            return ret;
        }

        /**
         * Close.
         **/
        public bool Close()
        {
            if (!mTransactionFlag)
            {
                if (mConnectNumber > 0)
                {
                    mConnectNumber--;
                }
            }

            if (mConnectNumber == 0)
            {
                mConnection.Close();
            }
            return true;
        }

        /**
         * Begin a transaction.
         **/
        public void BeginTransaction()
        {
            mSqlTransaction = mConnection.BeginTransaction(IsolationLevel.Serializable);
            mTransactionFlag = true;
        }

        /**
         * End a transaction.
         **/
        public void EndTransaction()
        {
            // command.Dispose()
            mSqlTransaction.Commit();
            mTransactionFlag = false;
            mConnection.Close();
            Close();
        }

        /**
         * If a transaction is failed call it.
         **/
        public void Rollback()
        {
            mSqlTransaction.Rollback();
        }

        /**
         * Update a record encapulated in the command.
         **/
        public int Update(NpgsqlCommand command)
        {
            // ...
            return 0;
        }

        /**
         * Insert a record encapulated in the command.
         **/
        public int Insert(NpgsqlCommand command)
        {
            int rowNumber = 0;
            try
            {
                command.Prepare();
                rowNumber = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                Close();
            }
            return rowNumber;
        }

        /**
         * Create command.
         **/
        public NpgsqlCommand CreateCommand(string strCommand)
        {
            NpgsqlCommand command = new NpgsqlCommand(strCommand, mConnection);

            if (mTransactionFlag)
            {
                command.Transaction = mSqlTransaction;
            }
            return command;
        }

        /**
         * Select encapulated in the command.
         **/
        public NpgsqlDataReader Select(NpgsqlCommand command)
        {
            command.Prepare();
            NpgsqlDataReader sqlReader = command.ExecuteReader();
            return sqlReader;
        }

        /**
         * Delete encapulated in the command.
         **/
        public bool Delete(NpgsqlCommand command)
        {
            command.Prepare();
            return command.ExecuteNonQuery() != 0;
        }

        /**
         * Execute encapulated in the command.
         **/
        public bool Execute(NpgsqlCommand command)
        {
            command.Prepare();
            return command.ExecuteNonQuery() != 0;
        }

        public String Language
        {
            get
            {
                return mLanguage;
            }
            set
            {
                mLanguage = value;
            }
        }
    }
}

