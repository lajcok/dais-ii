﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BenchmarkApp.ORM
{
    public class Event
    {
        public static string ATTR_id = "id";
        public static string ATTR_name = "name";
        public static string ATTR_price = "price";
        public static string ATTR_description = "description";
        public static string ATTR_manager_id = "manager_id";
        public static string ATTR_archive = "archive";


        public int? Id { get; set; } = null;
        public string Name { get; set; }
        public int Price { get; set; }
        public string Description { get; set; } = "";
        public int ManagerId { get; set; }
        public bool Archive { get; set; } = false;
        public DateTime TimeStart { get; set; }
        public int Duration { get; set; }

        public DateTime TimeEnd
        {
            get => TimeStart.AddMinutes(Duration);
            set => Duration = Convert.ToInt32(value.Subtract(TimeStart).TotalMinutes);
        }
        public override string ToString()
        {
            return string.Format("Event#{0} {1}", Id, Name);
        }
    }
}
