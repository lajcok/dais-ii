﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BenchmarkApp.ORM
{
    public class EventRepeat : Event
    {

        public DateTime RepeatFrom { get; set; }
        public DateTime? RepeatTo { get; set; } = null;
        public ISet<DayOfWeek> DaysOfWeek { get; } = new HashSet<DayOfWeek>();  // 0 = Sunday based

    }
}
