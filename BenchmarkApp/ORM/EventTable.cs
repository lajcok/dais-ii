﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Text;

namespace BenchmarkApp.ORM
{
    public class EventTable : DbTable
    {
        public static string TABLE_NAME = "event";
        public static string SQL_LIST = @"
SELECT id, name, price, description, manager_id, archive
FROM event";
        public static string SQL_GET = SQL_LIST + " WHERE id = :id";
        public static string SQL_PAGINATE = SQL_LIST + @"
WHERE manager_id = :manager_id AND NOT archive
LIMIT :per_page OFFSET :offset";
        public static string SP_EDIT_ONCE = @"
CALL CreateEventOnce(:name, :price, :description, :dt_from, :dt_to, :managerId, :eventId)";
        public static string SP_EDIT_REPEAT = @"
CALL CreateEventRepeat(
    :name, :price, :description,
    :time_start, :time_end, :weekdays, :repeat_start, :repeat_end,
    :managerId, :eventId
)";
        public static string SQL_ARCHIVE = @"
UPDATE event
SET archive = true
WHERE id = :id";

        public EventTable(Database database) : base(database, TABLE_NAME)
        {
        }

        /**
         * q16
         * Get Event by ID
         */
        public Event Get(int id)
        {
            mDatabase.Connect();
            var command = mDatabase.CreateCommand(SQL_GET);
            command.Parameters.AddWithValue(":id", id);
            var reader = mDatabase.Select(command);
            var events = Read(reader);
            reader.Close();
            mDatabase.Close();
            return events.Count == 1 ? events[0] : null;
        }

        /**
         * q17
         * List All Events
         */
        [Obsolete]
        public ICollection<Event> ListAll()
        {
            mDatabase.Connect();
            var command = mDatabase.CreateCommand(SQL_LIST);
            var reader = mDatabase.Select(command);
            var events = Read(reader);
            reader.Close();
            mDatabase.Close();
            return events;
        }

        /**
         * q17
         * List Paginated Events
         */
        public ICollection<Event> List(int managerId, int offset, int per_page)
        {
            mDatabase.Connect();
            var command = mDatabase.CreateCommand(SQL_PAGINATE);
            command.Parameters.AddWithValue(":manager_id", NpgsqlDbType.Integer, managerId);
            command.Parameters.AddWithValue(":per_page", NpgsqlDbType.Integer, per_page);
            command.Parameters.AddWithValue(":offset", NpgsqlDbType.Integer, offset);
            var reader = mDatabase.Select(command);
            var events = Read(reader);
            reader.Close();
            mDatabase.Close();
            return events;
        }

        // TODO q16 + q17: ID collection to obtain (for calendar)

        // TODO q19
        public bool Edit(Event @event)
        {
            mDatabase.Connect();
            var command = mDatabase.CreateCommand(@event is EventRepeat ? SP_EDIT_REPEAT : SP_EDIT_ONCE);
            command.Parameters.AddWithValue(":name", @event.Name);
            command.Parameters.AddWithValue(":price", @event.Price);
            command.Parameters.AddWithValue(":description", @event.Description);
            command.Parameters.AddWithValue(":managerId", @event.ManagerId);
            if (@event.Id != null) {
                command.Parameters.AddWithValue(":eventId", @event.Id);
            }
            else {
                command.Parameters.AddWithValue(":eventId", DBNull.Value);
            }
            if (@event is EventRepeat)
            {
                var eventRepeat = @event as EventRepeat;
                var daysOfWeek = new List<int>(eventRepeat.DaysOfWeek.Count);
                foreach (var dow in eventRepeat.DaysOfWeek)
                {
                    daysOfWeek.Add((int)dow + 1);
                }
                command.Parameters.AddWithValue(":time_start", NpgsqlDbType.Time, eventRepeat.TimeStart.TimeOfDay);
                command.Parameters.AddWithValue(":time_end", NpgsqlDbType.Time, eventRepeat.TimeEnd.TimeOfDay);
                command.Parameters.AddWithValue(":weekdays", NpgsqlDbType.Array | NpgsqlDbType.Integer, daysOfWeek.ToArray());
                command.Parameters.AddWithValue(":repeat_start", NpgsqlDbType.Date, eventRepeat.RepeatFrom);
                if (eventRepeat.RepeatTo != null)
                {
                    command.Parameters.AddWithValue(":repeat_end", NpgsqlDbType.Date, eventRepeat.RepeatTo);
                }
                else
                {
                    command.Parameters.AddWithValue(":repeat_end", DBNull.Value);
                }
            }
            else
            {
                command.Parameters.AddWithValue(":dt_from", @event.TimeStart);
                command.Parameters.AddWithValue(":dt_to", @event.TimeEnd);
            }
            bool success = mDatabase.Execute(command);
            mDatabase.Close();
            return success;
        }

        public bool TestEdit()
        {
            Random random = new Random();
            Event @event;
            if (random.NextDouble() < .5)
            {
                @event = new EventOnce();
            }
            else
            {
                var eventRepeat = new EventRepeat();
                eventRepeat.RepeatFrom = new DateTime(2019, random.Next(1, 12), random.Next(1, 25), random.Next(9, 20), 0, 0);
                for (int i = 0; i < 7; ++i)
                {
                    if (random.NextDouble() < .3)
                    {
                        eventRepeat.DaysOfWeek.Add((DayOfWeek)i);
                    }
                }
                @event = eventRepeat;
            }
            @event.Name = RandomString(20, random);
            @event.Price = random.Next(0, 10000);
            @event.Description = RandomString(20, random);
            @event.TimeStart = new DateTime(2019, random.Next(1, 12), random.Next(1, 25), random.Next(9, 20), 0, 0);
            @event.Duration = random.Next(30, 180);
            @event.ManagerId = random.Next(1, 500);
            if (random.NextDouble() < .5)
            {
                @event.Id = random.Next(0, 25000);
            }
            return Edit(@event);
        }

        private string RandomString(int length, Random random)
        {
            string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                result.Append(characters[random.Next(characters.Length)]);
            }
            return result.ToString();
        }

        /**
         * q20
         * Archive Event
         */
        public bool Archive(int eventId)
        {
            mDatabase.Connect();
            var command = mDatabase.CreateCommand(SQL_ARCHIVE);
            command.Parameters.AddWithValue(":id", eventId);
            bool success = mDatabase.Delete(command);
            mDatabase.Close();
            return success;
        }

        private Collection<Event> Read(NpgsqlDataReader reader)
        {
            var events = new Collection<Event>();
            while (reader.Read())
            {
                var e = new Event()
                {
                    Id = reader.GetInt32(0),
                    Name = reader.GetString(1),
                    Price = reader.GetInt32(2),
                    ManagerId = reader.GetInt32(4),
                    Archive = reader.GetBoolean(5),
                };
                if (!reader.IsDBNull(3))
                {
                    e.Description = reader.GetString(3);
                }
                events.Add(e);
            }
            return events;
        }
    }
}
