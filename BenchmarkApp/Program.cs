﻿using BenchmarkApp.ORM;
using System;
using System.Collections.Generic;

namespace BenchmarkApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** BenchmarkApp 0.1, Michal Kratky, March 2011 *****");

            /*
            var eventTable = new EventTable(new Database());
            var @event = eventTable.List(;
            Console.WriteLine(@event != null ? @event.ToString() : "None");
            */

            /*
            var calendarView = new CalendarView(new Database());
            Console.WriteLine(calendarView.Load(new DateTime(2018, 01, 01)).Count);
            */

            /*
            var eventTable = new EventTable(new Database());
            var @event = new EventRepeat()
            {
                Id = 25012,
                Name = "Muj event recurring",
                Price = 20,
                TimeStart = new DateTime(1, 1, 1, 10, 00, 00),
                Duration = 120,
                ManagerId = 1,
                RepeatFrom = new DateTime(2019, 01, 01),
                RepeatTo = new DateTime(2020, 01, 01),
            };
            @event.DaysOfWeek.Add(DayOfWeek.Monday);
            @event.DaysOfWeek.Add(DayOfWeek.Sunday);
            Console.WriteLine(eventTable.Edit(@event));
            */

            //return;

             int numberOfTestThreads = 10;
             new BenchmarkApp().Run(numberOfTestThreads);
        }
    }
}
