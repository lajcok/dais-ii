-- Implements function q19a

create or replace procedure CreateEventOnce(
    _name event.name%type,
    _price event.price%type,
    _description event.description%type,
    dt_from timestamp,
    dt_to timestamp,
    managerId event.manager_id%type default null,
    eventId event.id%type default null
) as $$
declare
    duration integer;
    metaId event_meta.id%type;
begin
    duration := extract(epoch from dt_to - dt_from) / 60;
    if duration <= 0 then
        raise exception 'Invalid begin % and end % date times', dt_from, dt_to;
    end if;

    if eventId is null then
        raise notice 'Creating new event';
        insert into event (name, price, description, manager_id)
        values (_name, _price, _description, managerId)
        returning id into eventId;
    else
        if not exists(select 1 from event where id = eventId) then
            raise notice 'Event % does not exist', eventId;
        end if;

        raise notice 'Cleaning event meta of %', eventId;
        delete from event_meta
        where event_id = eventId;

        raise notice 'Updating existing event %', eventId;
        update event
        set name=_name,
            price=_price,
            description=_description
        where id = eventId;
    end if;

    raise notice 'Creating init meta records';
    insert into event_meta (meta_key, meta_value, event_id)
    values ('repeat_once', dt_from::date, eventId)
    returning id into metaId;

    raise notice 'Creating the rest of meta records';
    insert into event_meta (meta_key, meta_value, event_id, meta_id)
    values ('start_time', dt_from::time, eventId, metaId),
           ('time_duration', duration, eventId, metaId);
end;
$$ language plpgsql;



-- Implements function q19b

create or replace procedure CreateEventRepeat (
    _name event.name%type,
    _price event.price%type,
    _description event.description%type,
    time_start time,
    time_end time,
    weekdays integer[],
    repeat_start date,
    repeat_end date,
    managerId event.manager_id%type default null,
    eventId event.id%type default null
)
as $$
declare
    duration integer;
    metaId event_meta.id%type;
begin
    duration := extract(epoch from time_end - time_start) / 60;
    if duration <= 0 then
        raise exception 'Invalid start % and end % times', time_start, time_end;
    end if;

    if eventId is null then
        raise notice 'Creating new event';
        insert into event (name, price, description, manager_id)
        values (_name, _price, _description, managerId)
        returning id into eventId;
    else
        if not exists(select 1 from event where id = eventId) then
            raise notice 'Event % does not exist', eventId;
        end if;

        raise notice 'Cleaning event meta of %', eventId;
        delete from event_meta
        where event_id = eventId;

        raise notice 'Updating existing event %', eventId;
        update event
        set name=_name,
            price=_price,
            description=_description
        where id = eventId;
    end if;

    raise notice 'Creating init meta records';
    insert into event_meta (meta_key, meta_value, event_id)
    values ('repeat_start', repeat_start::date, eventId)
    returning id into metaId;

    raise notice 'Creating the rest of common meta records';
    insert into event_meta (meta_key, meta_value, event_id, meta_id)
    values ('repeat_end', repeat_end, eventId, metaId),
           ('time_start', time_start, eventId, metaId),
           ('time_duration', duration, eventId, metaId);

    raise notice 'Creating weekday meta records';
    if weekdays is not null then
        insert into event_meta (meta_key, meta_value, event_id, meta_id)
        select distinct 'repeat_weekday', wd, eventId, metaId
        from unnest(weekdays) as wd;
    else
        insert into event_meta (meta_key, meta_value, event_id, meta_id)
        select distinct 'repeat_weekday', null, eventId, metaId;
    end if;
end;
$$ language plpgsql;



-- Implements integrity constraint for booking_participators

create or replace function ParticipatorOfCustomer(bookingId booking.id%type, participatorId participator.id%type)
    returns boolean as
$$
declare
    customerId customer.id%type;
    logins     integer[];
begin
    select customer_id
    into customerId
    from booking
    where id = bookingId;
    -- raise notice 'Obtained customer id %', customerId;

    select array_agg(p.login_id)
    into logins
    from participator as p
             left join customer as c
                       on p.id = c.id or c.id = customerId
    where p.id = participatorId;
    -- raise notice 'Login candidates %', logins;

    if array_length(logins, 1) <> 1 then
        return false;
    end if;

    if logins[1] is not null then
        return true;
    end if;

    return not exists(
            select 1
            from booking as b
                     inner join booking_participators as bp on b.id = bp.booking_id
            where bp.participator_id = participatorId
              and b.customer_id != customerId
        );
end;
$$ language plpgsql;

-- Applied as CHECK
/*
alter table booking_participators
add constraint chk_participator_of_customer
    check ( ParticipatorOfCustomer(booking_id, participator_id) );
 */

-- Applied as TRIGGER
create or replace function participator_of_customer_trigger()
    returns trigger
as $$
    begin
        if ParticipatorOfCustomer(new.booking_id, new.participator_id) then
            return new;
        else
            raise exception 'Participator % does not belong to customer of the booking %', new.participator_id, new.booking_id;
        end if;
    end;
$$ language plpgsql;

create trigger participator_of_customer_integrity
    before insert on booking_participators
    for each row    -- TODO Change to each statement for increased performance (function adjustments needed?)
    execute function participator_of_customer_trigger();
