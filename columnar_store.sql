create extension cstore_fdw;
create server cstore_server foreign data wrapper cstore_fdw;

-- 1

create foreign table booking_aggregate
(
    id          integer                             not null,
    customer_id integer                             not null,
    event_id    integer                             not null,
    event_date  date                                null,
    price       int                                 not null,
    booked_on   timestamp default current_timestamp not null,
    confirm     timestamp                           null,
    storno      timestamp                           null
)
server cstore_server
options (compression 'pglz');

insert into booking_aggregate (id, customer_id, event_id, event_date, price, booked_on, confirm, storno)
select id, customer_id, event_id, event_date, price, booked_on, confirm, storno
from booking;

-- drop foreign table booking_aggregate;

-- 1a - Sum of all prices
explain (analyze, buffers, timing)
select sum(price)
from booking;

-- 1a - Sum of all prices (columnar)
explain (analyze, buffers, timing)
select sum(price)
from booking_aggregate;

-- 1b - ... grouped by customer
explain (analyze, buffers, timing)
select sum(price)
from booking
group by customer_id;

-- 1b - ... grouped by customer (columnar)
explain (analyze, buffers, timing)
select sum(price)
from booking_aggregate
group by customer_id;

-- 2 - booked between ...
explain (analyze, buffers, timing)
select *
from booking
where confirm between '2018-01-01'::timestamp and '2019-01-01'::timestamp;

-- 2 - booked between (columnar)
explain (analyze, buffers, timing)
select *
from booking_aggregate
where confirm between '2018-01-01'::timestamp and '2019-01-01'::timestamp;

-- 3 - q18 columnar table
create foreign table event_meta_columnar
(
    id          integer     not null,
    meta_key    varchar(20) not null,
    meta_value  varchar(20) null,
    event_id    integer     not null,
    meta_id     integer
)
server cstore_server
options (compression 'pglz');

insert into event_meta_columnar (id, meta_key, meta_value, event_id, meta_id)
select id, meta_key, meta_value, event_id, meta_id
from event_meta;

-- q18 (columnar query)
explain (analyze, buffers, timing)
with dts as (
    select '2019-01-02'::date + generate_series as date
    from generate_series(0, '2019-01-02'::date - '2019-01-02'::date)
)
select distinct dts.date, ev.id as event_id
from dts, event as ev
         inner join event_meta_columnar as evm
                    on ev.id = evm.event_id
                        and evm.meta_key in ('repeat_start', 'repeat_once')
         left join event_meta_columnar as evre
                   on evre.meta_id = evm.id
                       and evre.meta_key = 'repeat_end'
         left join event_meta_columnar as evrwd
                   on evrwd.meta_id = evm.id
                       and evrwd.meta_key = 'repeat_weekday'
where (
        evm.meta_key = 'repeat_once'
        and evm.meta_value::date = dts.date
    )
   or (
        evm.meta_key = 'repeat_start'
        and evm.meta_value::date <= dts.date
        and (evre.meta_value is null or evre.meta_value::date >= dts.date)
        and (evrwd.meta_value is null or evrwd.meta_value::int = extract(isodow from dts.date))
    );
