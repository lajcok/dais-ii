-------------------
-- Reset
-------------------

drop table if exists compress_a;
drop table if exists compress_b;
drop foreign table if exists compress_c;
drop foreign table if exists compress_d;


-------------------
-- Compression OFF
-------------------

create table compress_a (
    id integer not null,
    txt text not null
);
alter table compress_a
alter txt set storage external;

create table compress_b (
    id integer not null,
    txt text not null
);
alter table compress_b
alter txt set storage external;

create foreign table compress_c (
    id integer not null,
    txt text not null
)
server cstore_server;
alter table compress_c              -- no effect
alter txt set storage external;

create foreign table compress_d (
    id integer not null,
    txt text not null
)
server cstore_server;
alter table compress_d              -- no effect
alter txt set storage external;


-------------------
-- Compression ON
-------------------

create table compress_a (
    id integer not null,
    txt text not null
);
alter table compress_a
alter txt set storage extended;

create table compress_b (
    id integer not null,
    txt text not null
);
alter table compress_b
alter txt set storage extended;

create foreign table compress_c (
    id integer not null,
    txt text not null
)
server cstore_server
options (compression 'pglz');
alter table compress_c              -- TODO no effect
alter txt set storage extended;

create foreign table compress_d (
    id integer not null,
    txt text not null
)
server cstore_server
options (compression 'pglz');
alter table compress_d              -- TODO no effect
alter txt set storage extended;


-------------------
-- Data
-------------------

insert into compress_a (id, txt)
select i, random_bytea(5e4::int)::text
from generate_series(1, 1e3::int) as i;

insert into compress_b (id, txt)
select i, repeat(case when random() < .5 then 'a' else 'b' end, 1e5::int)
from generate_series(1, 1e3::int) as i;

insert into compress_c (id, txt)
select id, txt
from compress_a;

insert into compress_d (id, txt)
select id, txt
from compress_b;


-------------------
-- Sizes
-------------------

-- (non)compress table sizes
select tbl, pg_table_size(tbl) as blocks, pg_size_pretty(pg_table_size(tbl)) as bytes
from unnest('{"compress_a", "compress_b"}'::text[]) as tbl
union
select tbl, cstore_table_size(tbl) as blocks, pg_size_pretty(cstore_table_size(tbl)) as bytes
from unnest('{"compress_c", "compress_d"}'::text[]) as tbl
order by tbl;


-------------------
-- Queries
-------------------

-- 1a
explain (analyze, buffers, timing)
select *
from compress_a;
-- 1b
explain (analyze, buffers, timing)
select *
from compress_b;
-- 1c
explain (analyze, buffers, timing)
select *
from compress_c;
-- 1d
explain (analyze, buffers, timing)
select *
from compress_d;

-- 2a
explain (analyze, buffers, timing)
select *
from compress_a
where id = 500;
-- 2b
explain (analyze, buffers, timing)
select *
from compress_b
where id = 500;
-- 2c
explain (analyze, buffers, timing)
select *
from compress_c
where id = 500;
-- 2d
explain (analyze, buffers, timing)
select *
from compress_d
where id = 500;

-- 3a
explain (analyze, buffers, timing)
select *
from compress_a
where txt like '%abcde%';
-- 3b
explain (analyze, buffers, timing)
select *
from compress_b
where txt like '%a%';
-- 3c
explain (analyze, buffers, timing)
select *
from compress_c
where txt like '%abcde%';
-- 3d
explain (analyze, buffers, timing)
select *
from compress_d
where txt like '%a%';

-- 4a
explain (analyze, buffers, timing)
select txt, count(id)
from compress_a
group by txt;
-- 4b
explain (analyze, buffers, timing)
select txt, count(id)
from compress_b
group by txt;
-- 4c
explain (analyze, buffers, timing)
select txt, count(id)
from compress_c
group by txt;
-- 4d
explain (analyze, buffers, timing)
select txt, count(id)
from compress_d
group by txt;

-- 5.1a
explain (analyze, buffers, timing)
insert into compress_a (id, txt)
select id, txt
from compress_a
limit 1;
-- 5.1b
explain (analyze, buffers, timing)
insert into compress_b (id, txt)
select id, txt
from compress_b
limit 1;

-- 5.2a
explain (analyze, buffers, timing)
update compress_a
set txt = (
        select txt
        from compress_a
        limit 1
)
where id = 500;
-- 5.2b
explain (analyze, buffers, timing)
update compress_b
set txt = (
        select txt
        from compress_b
        limit 1
)
where id = 500;

-- 5.3a
explain (analyze, buffers, timing)
delete from compress_a
where id = 400;
-- 5.3b
explain (analyze, buffers, timing)
delete from compress_b
where id = 400;
