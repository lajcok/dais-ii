-- Event System data generator

-- Git https://bitbucket.org/lajcok/dais-ii/src/master/generator.sql

-------------------------------------------------------------------------
-- customers                300e3
-- participators            cust + 10e3 = 310e3
-- managers                 500
-- logins                   cust * .2 + mngr = 60 500
-- events                   mngr * 20 = 10e3
-- event_meta               event * 4 = 40e3
-- bookings                 sel_cust(3e6), sel_evnt(3e6)  = ~2.5e6
-- booking_participators    book * avg_partic(1.8) = 3.5e6
-------------------------------------------------------------------------
-- total                    ~ 7e6
-------------------------------------------------------------------------

-- Time local   ~ 7m
-- Time VŠB     ~ 3m


-- Login

insert into login (email, password_hash, active)
select left(md5(random()::text), 15) || '@example.com' as email,
       left(md5(random()::text), 200)                  as password_hash,
       random() > .1                                   as active
from generate_series(1, 60500)
on conflict (email) do nothing -- skip duplicities
;


-- Manager

insert into manager (first_name, last_name, login_id)
select left(md5(random()::text), 30) as first_name,
       left(md5(random()::text), 30) as last_name,
       id                            as login_id
from login
order by random()::text -- spread on logins randomly
limit 500
on conflict (login_id) do nothing -- skip duplicities
;


-- Event

insert into event (name, price, manager_id, archive)
select left(md5(random()::text), 50) as name,
       floor(random() * 100) * 10    as price,
       id                            as manager_id,
       random() > .8                 as archive
from manager, generate_series(1, 20 + floor(random() * 60)::integer + 0 * id) -- variable amount of events per manager
;



-- Event Meta

with event_meta_init as ( -- event meta initialization records - meta keys: repeat_once, repeat_start
    insert into event_meta (meta_key, meta_value, event_id)
        select case
                   when random() > .5 then 'repeat_once' -- 50:50 chance between one-time and repeating event
                   else 'repeat_start'
                   end as meta_key,
               '2015-01-01'::date +
               floor(random() * ('2025-01-01'::date - '2015-01-01'::date))::integer -- a date in between
                       as meta_value,
               id      as event_id
        from event
        returning id as meta_id, meta_key, meta_value, event_id
),
     event_meta_common as ( -- parameters common for one-time and repeating events              -- TODO not executed!
         insert into event_meta (meta_key, meta_value, event_id, meta_id)
             select case
                        when gen_common = 1 then 'start_time'
                        when gen_common = 2 then 'time_duration'
                        end as meta_key,
                    case
                        when gen_common = 1
                            then (floor(random() * 144) * 600)::varchar::interval::time::varchar -- random start time 0:00 - 24:00 as varchar
                        when gen_common = 2
                            then (floor(random() * 96 + 1) * 10)::varchar -- random duration from 10 min up to 16 hours (as minutes)
                        end as meta_value,
                    event_id,
                    meta_id
             from event_meta_init,
                  generate_series(1, 2) as gen_common
     )
insert
into event_meta (meta_key, meta_value, event_id, meta_id)
select case
           when gen_repeat = 1 then 'repeat_end' -- first parameter
           else 'repeat_weekday' -- the rest is weekdays only
           end as meta_key,
       case
           when gen_repeat = 1 then -- repeat end
               case
                   when random() > .2 then (
                           meta_value::date + floor(random() * (date '2026-01-01' - meta_value::date))::integer
                       )::varchar
                   else null end -- repeat end can be null (endless repeating)
           else floor(random() * 7 + 1)::varchar -- days of week = random 1-7
           end as meta_value,
       event_id,
       meta_id
from event_meta_init
         inner join generate_series(1, 1 + 1 + floor(random() * 5)::integer) as gen_repeat -- variable number of weekdays to repeat on
                    on meta_key = 'repeat_start' -- additional parameters only for repeating events
;


-- Customer

with customer_logins as ( -- selection of logins to create customers for
    select login_id, login_email
    from (( -- existing logins
              select id as login_id, email as login_email
              from login
              order by random()
              limit 60e3
          )
          union all
          ( -- most of the customers are non-registered = without login...
              select null as login_id,
                     null as login_email -- ... => login id = null
              from generate_series(1, 240e3)
          )) as customer_generator
),
     customer_participators as ( -- record in participator table for every customer
         insert into participator (first_name, last_name, birth_date, restrictions, archived, login_id)
             select left(md5(random()::text), 10)                                        as first_name,
                    left(md5(random()::text), 10)                                        as last_name,
                    '1960-01-01'::date +
                    floor(random() * ('2005-01-01'::date - '1960-01-01'::date))::integer as birth_date, -- birth date from a range
                    case
                        when random() > .9 then left(md5(random()::text), 50)
                        else '' end                                                      as restrictions,
                    case
                        when random() > .9
                            then '2015-01-01'::timestamp
                            + (random() * extract(epoch from now() - '2015-01-01'::timestamp)) * interval '1 second'
                        else null end                                                    as archived,   -- some participators are marked archived
                    login_id
             from customer_logins
             returning id, login_id -- obtain ids of newly created records
     )
insert
into customer (id, contact_email, phone_number, address)
select cp.id                           as id,
       case
           when cl.login_email is not null and random() > .1
               then login_email -- if the customer is registered, use his login email as contact email (90% probability)
           else left(md5(random()::text), 15) || '@example.com' -- generate a random one otherwise
           end                         as contact_email,
       substr(random()::varchar, 3, 9) as phone_number,
       left(md5(random()::text), 30)   as address
from customer_participators as cp
         left join customer_logins as cl on cp.login_id = cl.login_id;


-- Bookings

with customers as ( -- customer selection for bookings
    select id                   as customer_id,
           login_id,
           generate_series,
           row_number() over () as rn -- row number for the later zip join
    from customer
             inner join participator as p using (id)
             cross join generate_series(1, case
                                               when p.login_id is not null then (random() * 200)::integer
                                               else 1 end) -- variable number of bookings per customer
    order by customer_id
    limit 3e6
),
     events as ( -- event selection for bookings
         select ev.id                                 as event_id,
                ev.price,
                evm.meta_key = 'repeat_start'         as is_repeat,
                row_number() over (order by random()) as rn -- row number for the later zip join
         from event as ev
                  inner join event_meta as evm
                             on evm.meta_key in ('repeat_once', 'repeat_start') and
                                ev.id = evm.event_id -- join to gather info about repeating
                  cross join generate_series(1, (random() * 500)::integer) -- variable number of bookings per event
         limit 3e6
     )
insert
into booking (event_date, price, confirm, storno, event_id, customer_id)
select case
           when (e.is_repeat and random() < .95) or (not e.is_repeat and random() < .05)
               then '2015-01-01'::date + floor(random() * ('2025-01-01'::date - '2015-01-01'::date))::integer
           else null end                       as event_date, -- event date specified only for recurring events
       case
           when random() < .99 then e.price
           else floor(random() * 100) * 10 end as price,      -- 1% chance of price inconsistency between event and its booking
       case
           when random() > .3
               then '2015-01-01'::timestamp
               + (random() * extract(epoch from now() - '2015-01-01'::timestamp)) *
                 interval '1 second' -- confirmation timestamp
           else null end                       as confirm,
       case
           when random() > .9
               then '2015-01-01'::timestamp
               + (random() * extract(epoch from now() - '2015-01-01'::timestamp)) *
                 interval '1 second' -- storno timestamp
           else null end                       as storno,
       e.event_id,
       c.customer_id
from customers as c
         join events as e on c.rn = e.rn -- zipping customers and events using row number
;

-- Booking participators

with participator_generator as ( -- participators for the booked event
    select c.id                                  as customer_id,
           p.login_id,
           generate_series = 1 and random() > .5 as include_customer, -- indicates whether the customer himself will participate on the event
           row_number() over ()                  as rn                -- row numbers to associate with to-be-created participators
    from customer as c
             inner join participator as p on c.id = p.id
             cross join generate_series(1, (random() * 3 + 0 * c.id)::integer) -- variable number of participators per booking (and customer)
),
     participators as ( -- generates participators based on the numbers from previous step
         insert into participator (first_name, last_name, birth_date, restrictions, archived, login_id)
             select left(md5(random()::text), 10)                                        as first_name,
                    left(md5(random()::text), 10)                                        as last_name,
                    '1960-01-01'::date +
                    floor(random() * ('2005-01-01'::date - '1960-01-01'::date))::integer as birth_date,
                    case
                        when random() > .9 then left(md5(random()::text), 50)
                        else '' end                                                      as restrictions,
                    case
                        when random() > .9
                            then '2015-01-01'::timestamp
                            + (random() * extract(epoch from now() - '2015-01-01'::timestamp)) * interval '1 second'
                        else null end                                                    as archived,
                    login_id
             from participator_generator
             returning id
     ),
     participators_numbered as ( -- number the results of the insert for zipping
         select id as participator_id, row_number() over () as rn
         from participators
     )
insert
into booking_participators (booking_id, participator_id)
select b.id as booking_id, participator_id
from booking as b
         inner join (
    ( -- participating customers
        select customer_id, customer_id as participator_id
        from participator_generator
        where include_customer
    )
    union
    ( -- generated participators
        select customer_id, participator_id
        from participator_generator
                 inner join participators_numbered using (rn) -- zipping newly inserted participators to the original generator
    )
) as bpg using (customer_id);

