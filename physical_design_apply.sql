-- q12
-- Materialized view tuning
create materialized view participator_customer
as select p.*, c.contact_email, c.phone_number, c.address
from participator as p
    left join customer as c using (id)
where p.archived is null;

-- q11
create index idx_participator_customer on participator_customer (id);

-- q17
create index idx_manager_id on event (manager_id);  -- index added

-- q18
create table events_on_date_cache as
with dts as (
    select '2018-10-01'::date + generate_series as date
    from generate_series(0, '2019-03-01'::date - '2018-10-01'::date)
)
select distinct dts.date, ev.id as event_id
from dts, event as ev
         inner join event_meta as evm
                    on
                            not ev.archive and
                            ev.id = evm.event_id and
                            evm.meta_key in ('repeat_start', 'repeat_once')
         left join event_meta as evre
                   on evre.meta_id = evm.id
                       and evre.meta_key = 'repeat_end'
         left join event_meta as evrwd
                   on evrwd.meta_id = evm.id
                       and evrwd.meta_key = 'repeat_weekday'
where (
        evm.meta_key = 'repeat_once'
        and evm.meta_value::date = dts.date
    )
   or (
        evm.meta_key = 'repeat_start'
        and evm.meta_value::date <= dts.date
        and (evre.meta_value is null or evre.meta_value::date >= dts.date)
        and (evrwd.meta_value is null or evrwd.meta_value::int = extract(isodow from dts.date))
    );

alter table events_on_date_cache
add primary key (date, event_id);

alter table events_on_date_cache
add foreign key (event_id) references event (id);

-- Optional index - for event specific calendar
create index idx_events_cache_id on events_on_date_cache (event_id);

-- alter q19a
create or replace procedure CreateEventOnce(
    _name event.name%type,
    _price event.price%type,
    _description event.description%type,
    dt_from timestamp,
    dt_to timestamp,
    managerId event.manager_id%type default null,
    eventId event.id%type default null
) as $$
declare
    duration integer;
    metaId event_meta.id%type;
begin
    duration := extract(epoch from dt_to - dt_from) / 60;
    if duration <= 0 then
        raise exception 'Invalid begin % and end % date times', dt_from, dt_to;
    end if;

    if eventId is null then
        raise notice 'Creating new event';
        insert into event (name, price, description, manager_id)
        values (_name, _price, _description, managerId)
        returning id into eventId;
    else
        if not exists(select 1 from event where id = eventId) then
            raise notice 'Event % does not exist', eventId;
        end if;

        raise notice 'Cleaning event meta of %', eventId;
        delete from event_meta
        where event_id = eventId;

        raise notice 'Updating existing event %', eventId;
        update event
        set name=_name,
            price=_price,
            description=_description
        where id = eventId;
    end if;

    raise notice 'Creating init meta records';
    insert into event_meta (meta_key, meta_value, event_id)
    values ('repeat_once', dt_from::date, eventId)
    returning id into metaId;

    raise notice 'Creating the rest of meta records';
    insert into event_meta (meta_key, meta_value, event_id, meta_id)
    values ('start_time', dt_from::time, eventId, metaId),
           ('time_duration', duration, eventId, metaId);

    delete from events_on_date_cache
    where event_id = eventId;

    insert into events_on_date_cache (date, event_id)
    values (dt_from::date, eventId);
end;
$$ language plpgsql;


-- alter q19b
create or replace procedure CreateEventRepeat (
    _name event.name%type,
    _price event.price%type,
    _description event.description%type,
    time_start time,
    time_end time,
    weekdays integer[],
    repeat_start date,
    repeat_end date,
    managerId event.manager_id%type default null,
    eventId event.id%type default null
)
as $$
declare
    duration integer;
    metaId event_meta.id%type;
begin
    duration := extract(epoch from time_end - time_start) / 60;
    if duration <= 0 then
        raise exception 'Invalid start % and end % times', time_start, time_end;
    end if;

    if eventId is null then
        raise notice 'Creating new event';
        insert into event (name, price, description, manager_id)
        values (_name, _price, _description, managerId)
        returning id into eventId;
    else
        if not exists(select 1 from event where id = eventId) then
            raise notice 'Event % does not exist', eventId;
        end if;

        raise notice 'Cleaning event meta of %', eventId;
        delete from event_meta
        where event_id = eventId;

        raise notice 'Updating existing event %', eventId;
        update event
        set name=_name,
            price=_price,
            description=_description
        where id = eventId;
    end if;

    raise notice 'Creating init meta records';
    insert into event_meta (meta_key, meta_value, event_id)
    values ('repeat_start', repeat_start::date, eventId)
    returning id into metaId;

    raise notice 'Creating the rest of common meta records';
    insert into event_meta (meta_key, meta_value, event_id, meta_id)
    values ('repeat_end', repeat_end, eventId, metaId),
           ('time_start', time_start, eventId, metaId),
           ('time_duration', duration, eventId, metaId);

    raise notice 'Creating weekday meta records';
    if weekdays is not null then
        insert into event_meta (meta_key, meta_value, event_id, meta_id)
        select distinct 'repeat_weekday', wd, eventId, metaId
        from unnest(weekdays) as wd;
    else
        insert into event_meta (meta_key, meta_value, event_id, meta_id)
        select distinct 'repeat_weekday', null, eventId, metaId;
    end if;


    delete from events_on_date_cache
    where event_id = eventId;

    with dts as (
        select repeat_start + generate_series as date
        from generate_series(0, '2019-03-01'::date - repeat_start)    -- Regenerate only for range
    )
    insert into events_on_date_cache (date, event_id)
    select distinct dts.date, ev.id
    from dts, event as ev
             inner join event_meta as evm
                        on
                                not ev.archive and
                                ev.id = evm.event_id and
                                evm.meta_key in ('repeat_start', 'repeat_once')
             left join event_meta as evre
                       on evre.meta_id = evm.id
                           and evre.meta_key = 'repeat_end'
             left join event_meta as evrwd
                       on evrwd.meta_id = evm.id
                           and evrwd.meta_key = 'repeat_weekday'
    where ev.id = eventId and ((
            evm.meta_key = 'repeat_once'
            and evm.meta_value::date = dts.date
        )
       or (
            evm.meta_key = 'repeat_start'
            and evm.meta_value::date <= dts.date
            and (evre.meta_value is null or evre.meta_value::date >= dts.date)
            and (evrwd.meta_value is null or evrwd.meta_value::int = extract(isodow from dts.date))
        ));
end;
$$ language plpgsql;

-- q19a, q19b
create index idx_meta_event_id on event_meta (event_id);

-- q22a - manager view
create index idx_booking_by_event on booking (event_id, event_date);    -- composite index

--q22b - customer view
create index idx_booking_by_customer on booking (customer_id);      -- index

-- q23b - non-registered
create index idx_bp_id on booking_participators (participator_id);    -- index
