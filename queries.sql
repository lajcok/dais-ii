-- Function list

-- Login

-- q1
select *
from login
where id = :id;

-- q2
select *
from login
where email = :email
  and password_hash = :password_hash
  and active;

-- q3
select *
from login;

-- q4
insert into login (email, password_hash)
values (:email, :password_hash);

-- q5
update login
set email         = :email,
    password_hash = :password_hash
where id = :id;

-- q6
update login
set active = false
where id = :id;


-- Manager

-- q7
select *
from manager
where id = :id;

-- q8
select *
from manager;

-- q9
insert into manager (first_name, last_name, login_id)
values (:first_name, :last_name, :login_id);

-- q10
update manager
set first_name = :first_name,
    last_name  = :last_name
where id = :id;


-- Participator, customer

-- q11
select p.*, c.*
from participator as p
         left join customer as c using (id)
where id = :id;

-- q12
select p.*, c.*
from participator as p
         left join customer as c using (id);

-- q13
insert into participator (first_name, last_name, birth_date, restrictions, login_id)
values (:first_name, :last_name, :birth_date, :restrictions, :login_id);

-- q14
insert into customer (id, contact_email, phone_number, address)
values (:participator_id, :contact_email, :phone_number, :address);


-- q15
update participator
set archived = current_timestamp
where id = :id;


-- Event (+ event_meta)

-- q16
select *
from event
where id = :id;

-- q17
select *
from event;

-- q18
with dts as (
    select :dt_from::date + generate_series as date
    from generate_series(0, :dt_to - :dt_from)
)
select distinct dts.date, ev.id as event_id
from dts, event as ev
         inner join event_meta as evm
                    on ev.id = evm.event_id
                        and evm.meta_key in ('repeat_start', 'repeat_once')
         left join event_meta as evre
                   on evre.meta_id = evm.id
                       and evre.meta_key = 'repeat_end'
         left join event_meta as evrwd
                   on evrwd.meta_id = evm.id
                       and evrwd.meta_key = 'repeat_weekday'
where (
        evm.meta_key = 'repeat_once'
        and evm.meta_value::date = dts.date
    )
   or (
        evm.meta_key = 'repeat_start'
        and evm.meta_value::date <= dts.date
        and (evre.meta_value is null or evre.meta_value::date >= dts.date)
        and (evrwd.meta_value is null or evrwd.meta_value::int = extract(isodow from dts.date))
    );

-- q19a - one-time
call CreateEventOnce(:name, :price, :description, :dt_from, :dt_to, :manager_id, :event_id);

-- q19b - repeating
call CreateEventRepeat(:name, :price, :description, :time_start, :time_end, :weekdays, :repeat_start, :repeat_end, :manager_id, :event_id);

-- q20
update event
set archive = true
where id = :id;


-- Booking

-- q21
select b.*, array_agg(bp.participator_id) as participator_ids
from booking as b
    inner join booking_participators as bp
            on b.id = bp.booking_id
where b.id = :id
group by b.id;

-- q22a - manager view
select *
from booking
where event_id = :event_id
  and event_date = :event_date;

--q22b - customer view
select *
from booking
where customer_id = :customer_id;

-- q23a - registered
do $$
    declare
        bookingId booking.id%type;
        eventPrice event.price%type;
    begin
        select price
        into eventPrice
        from event
        where id = :event_id;

        insert into booking (event_date, price, event_id, customer_id)
        values (:event_date, eventPrice, :event_id, :customer_id)
        returning id into bookingId;

        insert into booking_participators (booking_id, participator_id)
        values (bookingId, :partic_id1),      -- variable amount
               (bookingId, :partic_id2);
    end;
$$;

-- q23b - non-registered
do $$
    declare
        customerId customer.id%type;
        eventPrice event.price%type;
        bookingId  booking.id%type;
    begin
        insert into participator (first_name, last_name)
        values (:first_name, :last_name)
        returning id into customerId;

        insert into customer (id, contact_email, phone_number, address)
        values (customerId, :email, :phone_number, :address);

        select price
        into eventPrice
        from event
        where id = :event_id;

        insert into booking (event_id, event_date, price, customer_id)
        values (:event_id, :event_date, eventPrice, customerId)
        returning id into bookingId;

        with new_participators as (
            insert into participator (first_name, last_name)
                values (:pfn1, :pln1),      -- variable amount
                       (:pfn2, :pln2)
                returning id
        )
        insert
        into booking_participators (booking_id, participator_id)
        select bookingId as booking_id, p.id as participator_id
        from new_participators as p;
    end;
$$;

-- q24
update booking
set confirm = current_timestamp
where id = :id;

-- q25
update booking
set storno = current_timestamp
where id = :id;
