-- Function list

-- Login

-- q1
select *
from login
where id = 500;

-- q2
select *
from login
where email = 'noone@example.com'
  and password_hash = 'justtrying';

-- q3
select *
from login;

-- q4
insert into login (email, password_hash)
values ('trying@example.com', 'pass');

-- q5
update login
set email         = 'trying2@example.com',
    password_hash = 'pass'
where id = 60505;

-- q6
update login
set active = false
where id = 60505;


-- Manager

-- q7
select *
from manager
where id = 200;

-- q8
select *
from manager;

-- q9
insert into manager (first_name, last_name, login_id)
values ('The', 'Manager', 20);

-- q10
update manager
set first_name = 'The',
    last_name  = 'SecondManager'
where id = 20;


-- Participator, customer

-- q11
select p.*, c.*
from participator as p
         left join customer as c using (id)
where id = 10;

-- q12
select p.*, c.*
from participator as p
         left join customer as c using (id);

-- q13
insert into participator (first_name, last_name, birth_date, restrictions, login_id)
values ('Random', 'Name', '2000-01-01', '', 1)
returning id;

-- q14
insert into customer (id, contact_email, phone_number, address)
values (749315, 'fake@example.com', '123456789', 'Long Street 29');

-- q15
update participator
set archived = current_timestamp
where id = 749315;


-- Event (+ event_meta)

-- q16
select *
from event
where id = 100;

-- q17
select *
from event;

-- q18
explain (analyze, buffers, timing)
with dts as (
    select '2019-01-02'::date + generate_series as date
    from generate_series(0, '2019-01-02'::date - '2019-01-02'::date)
)
select distinct dts.date, ev.id as event_id
from dts, event as ev
         inner join event_meta as evm
                    on ev.id = evm.event_id
                        and evm.meta_key in ('repeat_start', 'repeat_once')
         left join event_meta as evre
                   on evre.meta_id = evm.id
                       and evre.meta_key = 'repeat_end'
         left join event_meta as evrwd
                   on evrwd.meta_id = evm.id
                       and evrwd.meta_key = 'repeat_weekday'
where (
        evm.meta_key = 'repeat_once'
        and evm.meta_value::date = dts.date
    )
   or (
        evm.meta_key = 'repeat_start'
        and evm.meta_value::date <= dts.date
        and (evre.meta_value is null or evre.meta_value::date >= dts.date)
        and (evrwd.meta_value is null or evrwd.meta_value::int = extract(isodow from dts.date))
    );
-- q19a - one-time

SET auto_explain.log_nested_statements = ON;

call CreateEventOnce('Test', 0, 'This is a test', '2019-12-01 12:30', '2019-12-01 13:30', 20);

insert into event (name, price, description, manager_id)
values ('Test', 100, '', 1)
returning id;

update event
set name='Test2',
	price=200,
	description=''
where id = 25005;

insert into event_meta (meta_key, meta_value, event_id)
values ('repeat_once', '2019-01-01', 25005)
returning id;

insert into event_meta (meta_key, meta_value, event_id, meta_id)
values ('start_time', '11:00:00', 25005, 149121),
	   ('time_duration', '60', 25005, 149121);

delete from event_meta
where event_id = 25005;



-- q19b - repeating
call CreateEventRepeat(:name, :price, :description, :time_start, :time_end, :weekdays, :repeat_start, :repeat_end, :manager_id, :event_id);

-- q20
update event
set archive = true
where id = 100;


-- Booking

-- q21
select b.*, array_agg(bp.participator_id) as participator_ids
from booking as b
    inner join booking_participators as bp
            on b.id = bp.booking_id
where b.id = 3000001
group by b.id;

-- q22a - manager view
select *
from booking
where event_id = 20
  and event_date is null;

--q22b - customer view
select *
from booking
where customer_id = 20;

-- q23a - registered
do $$
    declare
        bookingId booking.id%type;
        eventPrice event.price%type;
    begin
        select price
        into eventPrice
        from event
        where id = 10;

        insert into booking (event_date, price, event_id, customer_id)
        values (null, 100, 10, 200)
        returning id;

        insert into booking_participators (booking_id, participator_id)
        values (3000009, 302166),      -- variable amount
               (3000009, 200);
    end;
$$;

-- q23b - non-registered
do $$
    declare
        customerId customer.id%type;
        eventPrice event.price%type;
        bookingId  booking.id%type;
    begin
        insert into participator (first_name, last_name)
        values ('Test', 'Partic')
        returning id;

        insert into customer (id, contact_email, phone_number, address)
        values (749318, 'test@example.com', '12345789', 'Long Street 21');

        select price
        from event
        where id = 10;

        insert into booking (event_id, event_date, price, customer_id)
        values (10, null, 100, 749318)
        returning id;

        with new_participators as (
            insert into participator (first_name, last_name)
                values ('Test', 'Once'),      -- variable amount
                       ('Test', 'Two')
                returning id
        )
        insert
        into booking_participators (booking_id, participator_id)
        select 3000010 as booking_id, p.id as participator_id
        from new_participators as p;
    end;
$$;

-- q24
update booking
set confirm = current_timestamp
where id = 3000010;

-- q25
update booking
set storno = current_timestamp
where id = 3000010;
