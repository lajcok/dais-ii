create table login
(
    id            serial primary key,
    email         varchar(255)         not null unique,
    password_hash char(255)            not null,
    active        boolean default true not null
);

create table manager
(
    id         serial primary key,
    first_name varchar(50)                   not null,
    last_name  varchar(50)                   not null,
    login_id   integer references login (id) not null unique
);

create table event
(
    id          serial primary key,
    name        varchar(200)                    not null,
    price       integer                         not null,
    description text    default ''              not null,
    archive     boolean default false           not null,
    manager_id  integer references manager (id) not null
);

create table event_meta
(
    id         serial primary key,
    meta_key   varchar(20)                        not null,
    meta_value varchar(20)                        null,
    event_id   integer references event (id)      not null,
    meta_id    integer references event_meta (id) null
);

create table participator
(
    id           serial primary key,
    first_name   varchar(50)                         not null,
    last_name    varchar(50)                         not null,
    birth_date   date                                null,
    restrictions text   default ''                not null,
    updated      timestamp default current_timestamp not null,
    archived     timestamp                           null,
    login_id     integer references login (id)       null
);

create table customer
(
    id            integer      not null primary key,
    contact_email varchar(255) not null,
    phone_number  char(9)      not null,
    address       varchar      not null,
    foreign key (id) references participator (id)
);

create table booking
(
    id          serial primary key,
    event_date  date                                null,
    price       int                                 not null,
    confirm     timestamp                           null,
    storno      timestamp                           null,
    booked_on   timestamp default current_timestamp not null,
    event_id    integer references event (id)       not null,
    customer_id integer references customer (id)    not null
);

create table booking_participators
(
    booking_id      integer references booking (id),
    participator_id integer references participator (id),
    primary key (booking_id, participator_id)
)
